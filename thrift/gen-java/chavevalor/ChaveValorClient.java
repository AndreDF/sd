import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import java.util.Scanner;

public class ChaveValorClient {
  public static void main(String [] args) {

    try {
      TTransport transport;
     
      transport = new TSocket("localhost", 9090);
      transport.open();

      TProtocol protocol = new  TBinaryProtocol(transport);
      ChaveValor.Client client = new ChaveValor.Client(protocol);
	  
	  Scanner scan = new Scanner(System.in);
 	  while(servico != "4"){

		  System.out.println("Digite qual serviço executar:");
		  System.out.println("1. Busca Valor");
		  System.out.println("2. Insere Valor");
		  System.out.println("3. Remove Valor");
		  System.out.println("4. Sair");

	  	  String servico = scan.nextLine();

		  switch(servico){
			case "1":
				busca(client);
			case "2":
				insere(client);
			case "3":
				remove(client);
			case "4":
				System.out.println("Encerrando...");
				break;
			default:
				System.out.println("Opção inválida! Tente novamente.");
		  }
	  }

      transport.close();
    } catch (TException x) {
      x.printStackTrace();
    } 
  }

  private static void busca(ChaveValor.Client client) throws TException
  {
    Scanner scan = new Scanner(System.in);
	
    System.out.print("Digite uma chave: ");
    int chave = scan.nextLine();
	
	String valor = client.getKV(chave);
    System.out.println("Valor: " + valor);
  }

  private static void insere(ChaveValor.Client client) throws TException
  {
    Scanner scan = new Scanner(System.in);
	
    System.out.print("Digite uma chave: ");
    int chave = scan.nextLine();
		System.out.print("Digite um valor: ");
		String valor = scan.nextLine();

		String old = client.setKV(chave, valor);		
		System.out.println("Inserido com sucesso");
    if(old != null){ System.out.println("Valor antigo: " + old); }
  }

  private static void remove(ChaveValor.Client client) throws TException
  {
    Scanner scan = new Scanner(System.in);
	
    System.out.print("Digite uma chave: ");
    int chave = scan.nextLine();
	
	client.delKV(chave);
    System.out.println("Removido com sucesso");
  }
}
